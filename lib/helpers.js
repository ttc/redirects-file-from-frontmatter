import fs from 'fs'
import fm from 'front-matter'
import path from 'path'
import _ from 'lodash'

const isArgument = arg => {
  return new Promise((resolve, reject) => {
    if (arg) resolve(arg)
    else reject('Missing argument.')
  })
}

const fileExists = pathToFile => {
  return new Promise((resolve, reject) => {
    fs.stat(pathToFile, (err, stats) => {
      if (stats.isFile()) resolve(true)
      else reject(false)
    })
  })
}

const dirExists = pathToDir => {
  return new Promise((resolve, reject) => {
    fs.stat(pathToDir, (err, stats) => {
      if (stats.isDirectory()) resolve(true)
      else reject(false)
    })
  })
}

const isMarkdownFile = file => {
  return new Promise((resolve, reject) => {
    if (!_.isEmpty(file)) {
      if (file.toString().endsWith('.md')) resolve(true)
      else reject(new Error(`${file} is not a markdown file.`))
    } else reject(new Error('isMarkdownFile arg cannot be empty.'))
  })
}

const readFile = (file, encoding = 'utf8') => {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(file)) reject(new Error('readFile arg cannot be empty.'))
    fs.readFile(file, encoding, (err, data) => {
      if (err) reject(err)
      if (data) {
        resolve(data)
      } else reject(new Error('No data'))
    })
  })
}

const readDir = directory => {
  return new Promise((resolve, reject) => {
    if (!_.isEmpty(directory)) {
      fs.readdir(directory, (err, contents) => {
        if (err) reject(err)
        if (contents) {
          resolve(contents)
        } else reject(new Error('Directory has no content to return.'))
      })
    } else reject(new Error('readDir arg cannot be empty'))
  })
}

const readFrontmatter = data => {
  return new Promise((resolve, reject) => {
    if (!data) reject(new Error(`Data is empty or ${data} does not exist.`))
    const frontmatter = fm(data)
    if (frontmatter) resolve(frontmatter)
    else reject(new Error('No frontmatter'))
  })
}

const createRedirects = (from, to) => {
  return new Promise((resolve, reject) => {
    if (from && to) resolve(`"${from}" "${to}"`)
    else reject(new Error('createRedirects needs a from and a to argument.'))
  })
}

const appendFile = (file, data) => {
  fs.appendFile(file, data)
}

module.exports = {
  readDir,
  isMarkdownFile,
  readFile,
  readFrontmatter,
  createRedirects,
  appendFile,
  isArgument,
  dirExists,
  fileExists
}
