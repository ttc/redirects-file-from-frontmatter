#! /usr/bin/env node

import _ from 'lodash'
import shell from 'shelljs'
import redirects from './redirects'

let outputFile = ''
let sourceDir = ''
let from = ''
let to = ''

const isArgument = (arg, target) => {
  if (_.startsWith(arg, target)) {
    return true
  }
}

const getArgument = (arg) => {
  return _.split(arg, '=', 2)[1]
}

_.forEach(process.argv, arg => {
  if (isArgument(arg, '--outputFile')) outputFile = getArgument(arg)
  if (isArgument(arg, '--sourceDir')) sourceDir = getArgument(arg)
  if (isArgument(arg, '--from')) from = getArgument(arg)
  if (isArgument(arg, '--to')) to = getArgument(arg)
})

redirects(sourceDir, outputFile, from, to)
// shell.exec("npm run --outputFile=" + outputFile +" --sourceDir=" + sourceDir + " --from=" + from + " --to=" + to + " redirects")
