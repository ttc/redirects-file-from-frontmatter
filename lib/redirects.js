
import _ from 'lodash'
import path from 'path'

import {readDir, isMarkdownFile, readFile, readFrontmatter, createRedirects, appendFile, isArgument, dirExists, fileExists} from './helpers'

/* These variables are populated only when the using command line parameters in case this script gets triggered directly
 * f.e. using the package build script.
 */
const sourceDir_ = process.env.npm_config_sourceDir
const outputFile_ = process.env.npm_config_outputFile
const to_ = process.env.npm_config_to
const from_ = process.env.npm_config_from

const writeRedirectsToFile = (outputFile, from, to, dir = sourceDir) => {
  console.log(`Checking contents of ${dir}...`)
  readDir(dir).then(contents => {
    _.forEach(contents, c => {
      c = path.resolve(dir, c)
      isMarkdownFile(c).then(isMarkdown => {
        console.log(`${c} seems to be a markdown file.`)
        readFile(c).then(data => {
          readFrontmatter(data).then(frontmatter => {
            if (frontmatter) {
              const from_ = frontmatter.attributes[from]
              const to_ = frontmatter.attributes[to]
              if (from_ && to_) {
                _.forEach(from_, f => {
                  if (to_ && to_ !== f) {
                    createRedirects(f, to_)
                      .then(redirect => {
                        counter++
                        console.log(counter + ': ' + redirect)
                        appendFile(outputFile, redirect + '\n', err => {
                          console.log(err)
                        })
                      })
                      .catch(e => console.log(e))
                  }
                })
              }
            }
          }).catch(e => console.log(e))
        }).catch(e => console.log(e))
      }).catch(e => {
        console.log(`${c} is not a markdown file. Trying to descend recursively into folder...`)
        writeRedirectsToFile(outputFile, from, to, c)
      })
    })
  }).catch(e => console.log(e))
}

let counter = 0

// Check script arguments and if source and destination exist and then create redirects
const redirects = (sourceDir, outputFile, from, to) => {
  console.log(`Running script with these parameters: sourceDir=${sourceDir}, outputFile=${outputFile}, from=${from}, to=${to}.`)
  isArgument(from)
    .then(arg => {
      from = arg
      isArgument(to)
        .then(arg => {
          to = arg
          dirExists(sourceDir)
            .then(res => {
              if (res===true) {
                fileExists(outputFile).then(res => {
                  // Trigger the redirects creation
                  if (res===true) {
                    writeRedirectsToFile(outputFile, from, to, sourceDir)
                  }
                  else console.log("Output file does not exist. Please provide a destination file as an argument when running the script and make sure the file exists.")
                }).catch(e => console.log(e))
              }
              else console.log("Directory to parse does not exist. Please provide a source directory as an argument when running the script and make sure the directory exists.")
            }).catch(e => console.log(e))
        }).catch(e => console.log(e))
    }).catch(e => console.log(e))
}

// The redirects function is called directly when running this script or exported for use in external app
redirects(sourceDir_, outputFile_, from_, to_)
module.exports = redirects
