
# Create redirects from frontmatter in all markdown files in a given directory

Redirects have the following format:

"from/path" "to/path"

and are written to an external file.

## Run

Run with the following arguments:

```createredirects --outputFile=/path/to/output/redirects.txt --sourceDir=/path/to/markdownfiles/ --from=alias --to=slug```

## Arguments:

* outputFile: the file in which redirects are written. Please note that redirects are appended on each run. This script does not delete the output file after each run.

* sourceDir: the source directory which is recursively parsed for files ending with .md. Files with other extensions are ignored.

* from: the frontmatter key which holds the from part in the redirect.  This has to be an array in the frontmatter.Example:

```
alias:
  - /from/source1
  - /from/source2

```

* to: the frontmatter key which holds the to part in the redirect. Example:

You can target a frontmatter key like ```slug: /my/destination``` using ```--from=slug``` parameter when running the build script.


Using the to and from examples above, the following redirects would be created:

"/from/source1" "/my/destination"

"/from/source2" "/my/destination"
